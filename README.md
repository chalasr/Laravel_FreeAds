FreeAds App - Made with Laravel 4
================

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/166375a2-dea5-44d9-aeca-a0b58daf3b52/small.png)](https://insight.sensiolabs.com/projects/166375a2-dea5-44d9-aeca-a0b58daf3b52)

[Demo on freeads.chalasdev.fr](http://freeads.chalasdev.fr)

This application was created by Robin CHALAS - FullStack Web Developer -  http://www.chalasdev.fr/

Problems? Issues?
--------------

Write a message on chalasdev.fr or create an issue

This application requires:
-------------

- PHP >= 5.4
- Composer
- Npm

Getting Started
---------------

  - Clone this repository

  ``` git clone https://github.com/chalasr/Laravel_Freeads.git ```

  - Install vendor using composer

  ``` composer install ```

  - Create database

  ``` create db named 'freeads' on phpMyAdmin and import file 'freeads.sql' file ```

  - Start server

  ``` php artisan serve --port=3000 ```

Enjoy ! (on localhost:3000)

Credits
-------

Author : [Robin Chalas](http://www.chalasdev.fr/)

License
-------

Copyright (c) 2014-2015 [Robin Chalas](http://www.chaladev.fr/)
[![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html)
